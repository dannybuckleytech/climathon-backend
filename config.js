/**
 * enviroment configuration, it uses default values if they are not defined on the nodejs process.
 */
module.exports = {
  DOCKER_MODE: process.env.DOCKER_MODE || false,
  ENV: process.env.ENV || "dev",
  PORT: process.env.PORT || 7002,
  SERVICE_PROJECT: process.env.SERVICE_PROJECT || "climathon",
  SERVICE_NAME: process.env.SERVICE_NAME || "backend",
  REGISTRY_SERVICE: process.env.REGISTRY_SERVICE || "registry",
  REGISTRY_PORT: process.env.REGISTRY_PORT || 7001,
  SECRETS_FILE: process.env.SECRETS_FILE || "appSecrets.json",
  EMAILER_BUCKETNAME: process.env.EMAILER_BUCKETNAME || "engagingtech",
  EMAILER_LOCATION: process.env.EMAILER_LOCATION || "emailer/Dev/pending",
  SURVEY_EXPORT_SOURCE_EMAIL:
    process.env.SURVEY_EXPORT_SOURCE_EMAIL || "support@engaging.works",
  SURVEY_EXPORT_DEST_EMAIL:
    process.env.SURVEY_EXPORT_DEST_EMAIL || "helton@engaging.tech",
  SURVEY_EXPORT_BUCKETNAME: process.env.SURVEY_EXPORT_BUCKETNAME || "engaging-works",
  SURVEY_EXPORT_DIR: process.env.SURVEY_EXPORT_DIR || "dev/survey-exports"
};
