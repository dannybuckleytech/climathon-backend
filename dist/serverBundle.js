module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./server.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./config.js":
/*!*******************!*\
  !*** ./config.js ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * enviroment configuration, it uses default values if they are not defined on the nodejs process.
 */
module.exports = {
  DOCKER_MODE: process.env.DOCKER_MODE || false,
  ENV: process.env.ENV || "dev",
  PORT: process.env.PORT || 7002,
  SERVICE_PROJECT: process.env.SERVICE_PROJECT || "climathon",
  SERVICE_NAME: process.env.SERVICE_NAME || "backend",
  REGISTRY_SERVICE: process.env.REGISTRY_SERVICE || "registry",
  REGISTRY_PORT: process.env.REGISTRY_PORT || 7001,
  SECRETS_FILE: process.env.SECRETS_FILE || "appSecrets.json",
  EMAILER_BUCKETNAME: process.env.EMAILER_BUCKETNAME || "engagingtech",
  EMAILER_LOCATION: process.env.EMAILER_LOCATION || "emailer/Dev/pending",
  SURVEY_EXPORT_SOURCE_EMAIL:
    process.env.SURVEY_EXPORT_SOURCE_EMAIL || "support@engaging.works",
  SURVEY_EXPORT_DEST_EMAIL:
    process.env.SURVEY_EXPORT_DEST_EMAIL || "helton@engaging.tech",
  SURVEY_EXPORT_BUCKETNAME: process.env.SURVEY_EXPORT_BUCKETNAME || "engaging-works",
  SURVEY_EXPORT_DIR: process.env.SURVEY_EXPORT_DIR || "dev/survey-exports"
};


/***/ }),

/***/ "./schemas/common.js":
/*!***************************!*\
  !*** ./schemas/common.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

const { gql } = __webpack_require__(/*! apollo-server */ "apollo-server");
const { GraphQLModule } = __webpack_require__(/*! @graphql-modules/core */ "@graphql-modules/core");
const { PubSub } = __webpack_require__(/*! apollo-server */ "apollo-server");
const { CarSimulator } = __webpack_require__(/*! ../utils/CarSimulator */ "./utils/CarSimulator.js");

const pubsub = new PubSub();

const CAR_SIMULATOR_STREAM = "CAR_SIMULATOR_STREAM";
const carSimulator = new CarSimulator(1, pubsub, CAR_SIMULATOR_STREAM);

carSimulator.startSimulation(100, 500, 20000);

const typeDefs = gql`
  type Message {
    id: ID!
    user: String!
    content: String!
  }

  type EmissionData {
    pm1: Int
    pm2: Int
    pm10: Int
  }

  type Car {
    uuid: ID!
    latitude: Float!
    longitude: Float!
    emissionData: EmissionData
  }

  type Query {
    messages: [Message!]
  }

  type Mutation {
    stopStream: Boolean
  }

  type Subscription {
    streamCarData: [Car]
  }
`;

const resolvers = {
  Query: {
    messages: () => null
  },
  Mutation: {
    stopStream: (parent, {}) => {
      // const id = messages.length;
      // messages.push({
      //   id, user, content
      // })
      carSimulator.stopSimulation();
      // pubsub.publish(CAR_SIMULATOR_STREAM, {streamMessages: messages});
      // subscribers.forEach((fn) => fn())
      return true;
    }
  },
  Subscription: {
    streamCarData: {
      subscribe: () => {
        // onMessagesUpdates(() => pubsub.publish(POST_ADDED, { messages }));
        // setTimeout(() => pubsub.publish(CAR_SIMULATOR_STREAM, { messages }), 0);
        return pubsub.asyncIterator([CAR_SIMULATOR_STREAM]);
      }
    }
  }
};

module.exports = new GraphQLModule({
  typeDefs,
  resolvers,
  context: session => session
});


/***/ }),

/***/ "./schemas/index.js":
/*!**************************!*\
  !*** ./schemas/index.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

const common = __webpack_require__(/*! ./common */ "./schemas/common.js");

module.exports = {
  schemaModules: [
    common
  ]
};


/***/ }),

/***/ "./server.js":
/*!*******************!*\
  !*** ./server.js ***!
  \*******************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

const { ApolloServer } = __webpack_require__(/*! apollo-server */ "apollo-server");
const { buildFederatedSchema } = __webpack_require__(/*! @apollo/federation */ "@apollo/federation");
const { GraphQLModule } = __webpack_require__(/*! @graphql-modules/core */ "@graphql-modules/core");

// const mongoose = require("mongoose");
// const aws = require("aws-sdk");
const fs = __webpack_require__(/*! fs */ "fs");
const config = __webpack_require__(/*! ./config */ "./config.js");


// ──────────────────────── COORDINATE CONVERTER ───────────────────────────────────────────────────────

// const {starts} = require("./utils/coordinateConverter/coordinateConverter")

// // starts()





// const axios = require("axios");
// const dataLoader = require("./dataLoader/loader");

// const cronTasks = require("./cron/index");
// const surveyResultsLoader = require("./workers/submissionResults/loader");

// const directives = require("./directives/index");

// const startWorkerProcess = () => {
//   //run survey results process every 5s
//   setInterval(surveyResultsLoader.processSubmissions, 5000);
// };

/**
 * Merge all subschemas into a single main one.
 */
const { schemaModules } = __webpack_require__(/*! ./schemas */ "./schemas/index.js");

const mergeSchemas = () =>
  new GraphQLModule({
    imports: schemaModules
  });

// const initialiseAWS = credentials => aws.config.update(credentials);

/**
 * Retrieves the json secrets file
 * @param {*} fileName
 */
const getAppSecrets = secretsFileName => {
  let content = null;
  try {
    let filePath = config.DOCKER_MODE
      ? `/run/secrets/${secretsFileName}`
      : secretsFileName;
    console.log("secret file path", filePath);
    let strContent = fs.readFileSync(filePath, "utf-8");
    content = JSON.parse(strContent);
  } catch (error) {
    console.error(new Date(), "Unable to access secrets", error);
  }
  return content;
};

// const registryUrl = `http://${config.SERVICE_PROJECT}_${config.REGISTRY_SERVICE}_${config.ENV}:${config.REGISTRY_PORT}/register?type=graphql`;
// const serviceUrl = `http://${config.SERVICE_PROJECT}_${config.SERVICE_NAME}_${config.ENV}:${config.PORT}/graphql`;

const secrets = getAppSecrets(config.SECRETS_FILE);

// The split function takes three parameters:
//
// * A function that's called for each operation to execute
// * The Link to use for an operation if the function returns a "truthy" value
// * The Link to use for an operation if the function returns a "falsy" value

//////////////////////////////////////////

const { typeDefs, resolvers } = mergeSchemas();

const schema = buildFederatedSchema([{ typeDefs, resolvers }]);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req, connection }) => {
    if (connection) {
      // check connection for metadata
      return connection.context;
    } else {
      // check from req
      const token = req.headers.authorization || "";

      return { token };
    }
  }
});

server.listen({ port: config.PORT }).then(({ url, subscriptionsUrl }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});


/* harmony default export */ __webpack_exports__["default"] = (server.httpServer);


/***/ }),

/***/ "./utils/Car.js":
/*!**********************!*\
  !*** ./utils/Car.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const convertedCoordinates = __webpack_require__(/*! ./coordinateConverter/lib/convertedCoordinates.json */ "./utils/coordinateConverter/lib/convertedCoordinates.json");
const geolib = __webpack_require__(/*! geolib */ "geolib");
const axios = __webpack_require__(/*! axios */ "axios");

const getHome = () => {
  const { homes } = convertedCoordinates;
  return homes[Math.floor(Math.random() * homes.length)];
};

const getTargetLocation = home => {
  const { cities } = convertedCoordinates;
  const distancedMappedCities = cities.map(city => {
    const { cityName, options } = city;
    const selectedLocationCoordinates =
      options[Math.floor(Math.random() * options.length)];
    const calculatedDistance = geolib.getDistance(
      {
        latitude: selectedLocationCoordinates.latitude,
        longitude: selectedLocationCoordinates.longitude
      },
      { latitude: home.latitude, longitude: home.longitude }
    );
    return {
      cityName,
      longitude: selectedLocationCoordinates.longitude,
      latitude: selectedLocationCoordinates.latitude,
      distance: calculatedDistance
    };
  });
  return distancedMappedCities.sort(
    ({ distance: distanceA }, { distance: distanceB }) => distanceA - distanceB
  )[0];
};

const flattenRoute = routeData => {
  const flattenedRoute = [];
  routeData.routes[0].legs[0].steps.forEach((step, stepIndex) =>
    step.intersections.forEach((intersection, intersectionIndex) => {
      const combinedIndex = stepIndex + intersectionIndex / 10;
      flattenedRoute.push({
        index: combinedIndex,
        longitude: intersection.location[0],
        latitude: intersection.location[1],
        uuid: Math.floor(Math.random() * 10000000)
      });
    })
  );
  return flattenedRoute;
};

const orderFlattenedRoute = flattenedRoute => {
  return flattenedRoute.sort(({ indexA }, { indexB }) => indexA - indexB);
};

const getRoute = async (home, targetLocation) => {
  try {
    const res = await axios.get(
      `http://router.project-osrm.org/route/v1/driving/${home.longitude},${home.latitude};${targetLocation.longitude},${targetLocation.latitude}?overview=false&steps=true`
    );
    if (res.data) {
      return { rawRoute: res.data, flattenedRoute: flattenRoute(res.data) };
    }
    return false;
  } catch (err) {
    console.log("ERROR", err);
  }
  return false;
};

const moveDistance = ({
  tickDistance,
  currentLatitude,
  currentLongitude,
  flattenedRoute,
  currentRouteStepUuid,
  goingToTarget,
  stoppedTraveling,
  carUuid
}) => {

  const roundedCoordinate = coor => Math.round(coor * 100000000) / 100000000;
  const localGoingToTarget = goingToTarget;
  const localCurrent = {
    latitude: roundedCoordinate(parseFloat(currentLatitude)),
    longitude: roundedCoordinate(parseFloat(currentLongitude))
  };

  if (stoppedTraveling) {
    console.log(`CAR ${carUuid} STOPPED TRAVELING`)

    return {
      latitude: roundedCoordinate(localCurrent.latitude),
      longitude: roundedCoordinate(localCurrent.longitude),
      goingToTarget: localGoingToTarget,
      stoppedTraveling: true,
      currentRouteStepUuid: currentRouteStepUuid
    };
  } else {
    console.log(`CAR ${carUuid} DRIVING TO ${goingToTarget ? "WORK": "HOME"}`)
  }

  const calculateNextStep = scopeCurrentRouteStepUuid => {
    const currentRouteStepIndex = flattenedRoute.findIndex(
      step => step.uuid === scopeCurrentRouteStepUuid
    );

    return localGoingToTarget
      ? flattenedRoute[currentRouteStepIndex + 1]
      : flattenedRoute[currentRouteStepIndex - 1];
  };

  let nextStep = calculateNextStep(currentRouteStepUuid);

  if (!nextStep) {
    debugger;
    console.log("FAIL SAFE - REACHED END OF ROUTE");
    return {
      latitude: roundedCoordinate(localCurrent.latitude),
      longitude: roundedCoordinate(localCurrent.longitude),
      goingToTarget: !localGoingToTarget,
      stoppedTraveling: true,
      currentRouteStepUuid: currentRouteStepUuid
    };
  }

  //calculate move
  let stepComplete = false;
  const remainingMovement = {
    latitude: tickDistance,
    longitude: tickDistance
  };

  while (!stepComplete) {
  // console.log('---LOCAL CURRENT LAT', localCurrent.latitude)
  // console.log('---LOCAL CURRENT LONG', localCurrent.longitude)
    //Move to next step if reached current step
    if (
      roundedCoordinate(localCurrent.latitude) === roundedCoordinate(nextStep.latitude) &&
      roundedCoordinate(localCurrent.longitude) === roundedCoordinate(nextStep.longitude)
    ) {

      let savedStep = nextStep;
      nextStep = calculateNextStep(nextStep.uuid);

      if (!nextStep) {
        console.log("CAR REACHED END OF ROUTE, REVERSING, STOPPING", {
          latitude: roundedCoordinate(savedStep.latitude),
          longitude: roundedCoordinate(savedStep.longitude),
          goingToTarget: !goingToTarget,
          stoppedTraveling: true,
          currentRouteStepUuid: savedStep.uuid
        });

        return {
          latitude: roundedCoordinate(savedStep.latitude),
          longitude: roundedCoordinate(savedStep.longitude),
          goingToTarget: !localGoingToTarget,
          stoppedTraveling: true,
          currentRouteStepUuid: savedStep.uuid
        };
      } else {
        // console.log(
        //   "NEW STEP REACHED",
        //   nextStep.index,
        //   flattenedRoute[flattenedRoute.length - 1].index
        // );
      }
    }
    const requiredMovement = {
      latitude: Math.abs(
        parseFloat(localCurrent.latitude) - parseFloat(nextStep.latitude)
      ),
      longitude: Math.abs(
        parseFloat(localCurrent.longitude) - parseFloat(nextStep.longitude)
      )
    };

    const performCalc = type => {
      if (remainingMovement[type] >= Math.abs(requiredMovement[type])) {
        //Able to reach target within this cycle
        localCurrent[type] = nextStep[type];
        remainingMovement[type] -= requiredMovement[type];
      } else {
        // otherwise move in correct direction as much as possible
        if (localCurrent[type] >= nextStep[type]) {
          localCurrent[type] -= remainingMovement[type];
        } else {
          localCurrent[type] += remainingMovement[type];
        }
        remainingMovement[type] = 0;
      }
    };

    performCalc("latitude");
    performCalc("longitude");

    //check if at target
    const targetLocation = goingToTarget
      ? flattenedRoute[flattenedRoute.length - 1]
      : flattenedRoute[0];
    if (
      roundedCoordinate(localCurrent.latitude) ===
        roundedCoordinate(targetLocation.latitude) &&
      roundedCoordinate(localCurrent.longitude) ===
        roundedCoordinate(targetLocation.longitude)
    ) {

      return {
        latitude: roundedCoordinate(targetLocation.latitude),
        longitude: roundedCoordinate(targetLocation.longitude),
        goingToTarget: !localGoingToTarget,
        stoppedTraveling: true,
        currentRouteStepUuid: targetLocation.uuid
      };
    }
    // console.log('CYCLE COMPLETE')
    // console.log('LAT REMAINING', remainingMovement.latitude)
    // console.log('LANG REMAINING', remainingMovement.longitude)
    stepComplete = remainingMovement.latitude === 0 || remainingMovement.longitude === 0;
  }
  // console.log("COMPLETED MOVE", {
  //   latitude: roundedCoordinate(localCurrent.latitude),
  //   longitude: roundedCoordinate(localCurrent.longitude),
  //   goingToTarget: localGoingToTarget,
  //   stoppedTraveling: false,
  //   currentRouteStepUuid: nextStep.uuid
  // });
  return {
    latitude: roundedCoordinate(localCurrent.latitude),
    longitude: roundedCoordinate(localCurrent.longitude),
    goingToTarget: localGoingToTarget,
    stoppedTraveling: false,
    currentRouteStepUuid: nextStep.uuid
  };
};

const calculateEmissionValues = () => {
  return {
    pm1: Math.floor(Math.random() * 400000000) + 600000000,
    pm2: Math.floor(Math.random() * 300000000) + 500000000,
    pm10: Math.floor(Math.random() * 100000000) + 300000000
  }
}

class Car {
  constructor(tickDistance = 0.5) {
    this.uuid = Math.floor(Math.random() * 10000000);
    this.tickDistance = tickDistance;
    this.deactivate = false;
    this.emissionData = calculateEmissionValues()
  }

  async initialise() {
    this.home = getHome();
    this.getTargetLocation = getTargetLocation(this.home);
    this.route = await getRoute(this.home, this.getTargetLocation);
    this.latitude = this.home.latitude;
    this.longitude = this.home.longitude;
    this.currentRouteStepUuid = this.route ? this.route.flattenedRoute[0].uuid : null;
    this.goingToTarget = true;
    this.stoppedTraveling = false;
  }

  get getFlattenedRoute() {
    if (this.route) {
      return orderFlattenedRoute(this.route.flattenedRoute);
    }
    return false;
  }

  toggleTravelingStatus() {
    this.stoppedTraveling = !this.stoppedTraveling
  }

  newTick() {
    this.emissionData = calculateEmissionValues()
    if (this.route) {
      const {
        latitude,
        longitude,
        goingToTarget,
        stoppedTraveling,
        currentRouteStepUuid
      } = moveDistance({
        tickDistance: this.tickDistance,
        currentLatitude: this.latitude,
        currentLongitude: this.longitude,
        flattenedRoute: this.getFlattenedRoute,
        currentRouteStepUuid: this.currentRouteStepUuid,
        goingToTarget: this.goingToTarget,
        stoppedTraveling: this.stoppedTraveling,
        carUuid: this.uuid
      });
      this.latitude = latitude;
      this.longitude = longitude;
      this.goingToTarget = goingToTarget;
      this.stoppedTraveling = stoppedTraveling;
      this.currentRouteStepUuid = currentRouteStepUuid;
      return true;
    }
    this.deactivate = true;
    return false;
  }
}

module.exports = {
  Car
};


/***/ }),

/***/ "./utils/CarSimulator.js":
/*!*******************************!*\
  !*** ./utils/CarSimulator.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const {Car} = __webpack_require__(/*! ./Car */ "./utils/Car.js")
class CarSimulator {
  constructor(initialAmount, pubsub, broadcastFlag){
    this.amountOfCars = initialAmount
    this.pubsub = pubsub
    this.broadcastFlag = broadcastFlag
    this.active = false
    this.data = {number: 1}
    this.interval = null
    this.cars = []
    this.timeProcessed = 0
  }

  async startSimulation (amountOfCars = this.amountOfCars, frequency = 500, dayTransitionCycle = 5000){
    await this.createCars(amountOfCars)
    this.interval = setInterval(()=> {
      this.active = true
      this.cars.forEach((car) => car.newTick())
      this.timeProcessed += frequency
      if(this.timeProcessed % dayTransitionCycle === 0){
        console.log('TRANSITIONING DAY CYCLE')
        this.cars.forEach((car) => car.toggleTravelingStatus())
      }
      this.pubsub.publish(this.broadcastFlag, {streamCarData: this.cars});
    }, frequency)
  }

  async createCars (amount = 1){
    let amountLeft = amount
    while(amountLeft !== 0){
      const car = new Car(0.05)
      await car.initialise()
      this.cars.push(car)
      amountLeft -= 1
      console.log(amountLeft)
    }
  }

  clearCars(){
    this.cars = []
  }

  stopSimulation(){
    clearInterval(this.interval)
    this.clearCars()
    this.active = false
  }

}


module.exports = {
  CarSimulator
}

/***/ }),

/***/ "./utils/coordinateConverter/lib/convertedCoordinates.json":
/*!*****************************************************************!*\
  !*** ./utils/coordinateConverter/lib/convertedCoordinates.json ***!
  \*****************************************************************/
/*! exports provided: cities, homes, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"cities\":[{\"options\":[{\"latitude\":\"52.5225794\",\"longitude\":\"-2.01769456\"},{\"latitude\":\"52.38760077\",\"longitude\":\"-1.82437901\"},{\"latitude\":\"52.41728229\",\"longitude\":\"-2.05202795\"},{\"latitude\":\"52.5140973\",\"longitude\":\"-1.70652127\"},{\"latitude\":\"52.44069282\",\"longitude\":\"-1.96879403\"},{\"latitude\":\"52.58846228\",\"longitude\":\"-1.87184534\"},{\"latitude\":\"52.55318994\",\"longitude\":\"-1.79111163\"},{\"latitude\":\"52.57705177\",\"longitude\":\"-1.93647803\"},{\"latitude\":\"52.34565236\",\"longitude\":\"-1.93460899\"},{\"latitude\":\"52.35416629\",\"longitude\":\"-1.90613823\"},{\"latitude\":\"52.40388207\",\"longitude\":\"-1.84098489\"},{\"latitude\":\"52.51231842\",\"longitude\":\"-1.73298867\"},{\"latitude\":\"52.44617953\",\"longitude\":\"-2.00403435\"},{\"latitude\":\"52.3565086\",\"longitude\":\"-1.81457107\"},{\"latitude\":\"52.57385378\",\"longitude\":\"-1.94415057\"},{\"latitude\":\"52.39119827\",\"longitude\":\"-2.04616741\"},{\"latitude\":\"52.42736442\",\"longitude\":\"-1.76633811\"},{\"latitude\":\"52.51087837\",\"longitude\":\"-2.0044899\"},{\"latitude\":\"52.48665454\",\"longitude\":\"-1.65998037\"},{\"latitude\":\"52.45265706\",\"longitude\":\"-1.74834213\"}],\"cityName\":\"birmingham\"},{\"options\":[{\"latitude\":\"53.37814047\",\"longitude\":\"-1.32071446\"},{\"latitude\":\"53.36933049\",\"longitude\":\"-1.38123506\"},{\"latitude\":\"53.35988225\",\"longitude\":\"-1.41655466\"},{\"latitude\":\"53.3935578\",\"longitude\":\"-1.57052378\"},{\"latitude\":\"53.46863177\",\"longitude\":\"-1.42133805\"},{\"latitude\":\"53.28966095\",\"longitude\":\"-1.50508979\"},{\"latitude\":\"53.36057004\",\"longitude\":\"-1.30152678\"},{\"latitude\":\"53.37751288\",\"longitude\":\"-1.26316976\"},{\"latitude\":\"53.44180044\",\"longitude\":\"-1.4558044\"},{\"latitude\":\"53.40256257\",\"longitude\":\"-1.54323006\"},{\"latitude\":\"53.37291354\",\"longitude\":\"-1.34983862\"},{\"latitude\":\"53.4081641\",\"longitude\":\"-1.60056135\"},{\"latitude\":\"53.5092769\",\"longitude\":\"-1.37820358\"},{\"latitude\":\"53.43211917\",\"longitude\":\"-1.35230242\"},{\"latitude\":\"53.26455813\",\"longitude\":\"-1.39628491\"},{\"latitude\":\"53.49650795\",\"longitude\":\"-1.54098323\"},{\"latitude\":\"53.3272976\",\"longitude\":\"-1.51965897\"},{\"latitude\":\"53.45533669\",\"longitude\":\"-1.36784094\"},{\"latitude\":\"53.35811161\",\"longitude\":\"-1.34352095\"},{\"latitude\":\"53.51787828\",\"longitude\":\"-1.48793228\"}],\"cityName\":\"sheffield\"},{\"options\":[{\"latitude\":\"53.91258634\",\"longitude\":\"1.55048044\"},{\"latitude\":\"53.67795018\",\"longitude\":\"1.45244514\"},{\"latitude\":\"53.81840068\",\"longitude\":\"1.61508278\"},{\"latitude\":\"53.84937256\",\"longitude\":\"1.73190176\"},{\"latitude\":\"53.86750388\",\"longitude\":\"1.45437208\"},{\"latitude\":\"53.85274385\",\"longitude\":\"1.55067137\"},{\"latitude\":\"53.7857551\",\"longitude\":\"1.62507813\"},{\"latitude\":\"53.76931769\",\"longitude\":\"1.35116589\"},{\"latitude\":\"53.74203509\",\"longitude\":\"1.47705971\"},{\"latitude\":\"53.83365011\",\"longitude\":\"1.70486053\"},{\"latitude\":\"53.88000668\",\"longitude\":\"1.7503619\"},{\"latitude\":\"53.85011086\",\"longitude\":\"1.49032418\"},{\"latitude\":\"53.76769637\",\"longitude\":\"1.40038696\"},{\"latitude\":\"53.85156324\",\"longitude\":\"1.59134677\"},{\"latitude\":\"53.83837078\",\"longitude\":\"1.44725536\"},{\"latitude\":\"53.88658832\",\"longitude\":\"1.49888535\"},{\"latitude\":\"53.93434676\",\"longitude\":\"1.60380969\"},{\"latitude\":\"53.72977867\",\"longitude\":\"1.43405132\"},{\"latitude\":\"53.69984312\",\"longitude\":\"1.66869922\"},{\"latitude\":\"53.8761862\",\"longitude\":\"1.56233919\"}],\"cityName\":\"leeds\"},{\"options\":[{\"latitude\":\"50.76967162\",\"longitude\":\"-0.30952538\"},{\"latitude\":\"50.93912332\",\"longitude\":\"-0.18635285\"},{\"latitude\":\"50.95005499\",\"longitude\":\"-0.10911483\"},{\"latitude\":\"50.90424852\",\"longitude\":\"0.00623792\"},{\"latitude\":\"50.75206148\",\"longitude\":\"-0.07488914\"},{\"latitude\":\"50.78078344\",\"longitude\":\"-0.32049808\"},{\"latitude\":\"50.81586858\",\"longitude\":\"0.06056809\"},{\"latitude\":\"50.92316875\",\"longitude\":\"-0.05964439\"},{\"latitude\":\"50.89065219\",\"longitude\":\"-0.11264252\"},{\"latitude\":\"50.77404984\",\"longitude\":\"-0.34409214\"},{\"latitude\":\"50.76565311\",\"longitude\":\"-0.20669893\"},{\"latitude\":\"50.91476203\",\"longitude\":\"-0.30252265\"},{\"latitude\":\"50.8455849\",\"longitude\":\"-0.3474568\"},{\"latitude\":\"50.84600479\",\"longitude\":\"0.0588911\"},{\"latitude\":\"50.81164878\",\"longitude\":\"-0.08565482\"},{\"latitude\":\"50.71285646\",\"longitude\":\"-0.11992497\"},{\"latitude\":\"50.82262179\",\"longitude\":\"-0.15925477\"},{\"latitude\":\"50.78059602\",\"longitude\":\"0.00134934\"},{\"latitude\":\"50.73488321\",\"longitude\":\"-0.26390939\"},{\"latitude\":\"50.80163471\",\"longitude\":\"-0.03067857\"}],\"cityName\":\"brighton\"},{\"options\":[{\"latitude\":\"53.41132399\",\"longitude\":\"-2.28796833\"},{\"latitude\":\"53.54763066\",\"longitude\":\"-2.46675163\"},{\"latitude\":\"53.48451761\",\"longitude\":\"-2.4245036\"},{\"latitude\":\"53.57017429\",\"longitude\":\"-2.04689287\"},{\"latitude\":\"53.44602865\",\"longitude\":\"-2.34383589\"},{\"latitude\":\"53.53093196\",\"longitude\":\"-2.40725827\"},{\"latitude\":\"53.56818854\",\"longitude\":\"-2.34753804\"},{\"latitude\":\"53.53139638\",\"longitude\":\"-2.43403281\"},{\"latitude\":\"53.43011324\",\"longitude\":\"-2.20734397\"},{\"latitude\":\"53.42211351\",\"longitude\":\"-2.14166789\"},{\"latitude\":\"53.53747201\",\"longitude\":\"-2.1876888\"},{\"latitude\":\"53.54934125\",\"longitude\":\"-2.39337731\"},{\"latitude\":\"53.39692802\",\"longitude\":\"-2.38185293\"},{\"latitude\":\"53.53285318\",\"longitude\":\"-2.05646083\"},{\"latitude\":\"53.35816427\",\"longitude\":\"-2.33497868\"},{\"latitude\":\"53.62367233\",\"longitude\":\"-2.32936932\"},{\"latitude\":\"53.47558201\",\"longitude\":\"-2.06826667\"},{\"latitude\":\"53.47039327\",\"longitude\":\"-2.32190926\"},{\"latitude\":\"53.36412671\",\"longitude\":\"-2.22077479\"},{\"latitude\":\"53.46338953\",\"longitude\":\"-2.16587227\"}],\"cityName\":\"manchester\"},{\"options\":[{\"latitude\":\"51.59659164\",\"longitude\":\"0.06147994\"},{\"latitude\":\"51.57724156\",\"longitude\":\"0.06852319\"},{\"latitude\":\"51.49848249\",\"longitude\":\"-0.35364334\"},{\"latitude\":\"51.59677544\",\"longitude\":\"-0.19790726\"},{\"latitude\":\"51.50902716\",\"longitude\":\"-0.03880984\"},{\"latitude\":\"51.41904048\",\"longitude\":\"-0.27013549\"},{\"latitude\":\"51.47030398\",\"longitude\":\"-0.34009607\"},{\"latitude\":\"51.50592356\",\"longitude\":\"-0.11995076\"},{\"latitude\":\"51.43955499\",\"longitude\":\"-0.02304482\"},{\"latitude\":\"51.4643125\",\"longitude\":\"-0.06577931\"},{\"latitude\":\"51.43765196\",\"longitude\":\"-0.24458463\"},{\"latitude\":\"51.54070922\",\"longitude\":\"-0.18572104\"},{\"latitude\":\"51.54509903\",\"longitude\":\"-0.01104179\"},{\"latitude\":\"51.52926772\",\"longitude\":\"-0.02036587\"},{\"latitude\":\"51.59210209\",\"longitude\":\"-0.10878604\"},{\"latitude\":\"51.60581028\",\"longitude\":\"-0.27246143\"},{\"latitude\":\"51.47454963\",\"longitude\":\"-0.07680852\"},{\"latitude\":\"51.54284917\",\"longitude\":\"-0.03646331\"},{\"latitude\":\"51.49271025\",\"longitude\":\"-0.330495\"},{\"latitude\":\"51.56315055\",\"longitude\":\"-0.30687366\"}],\"cityName\":\"london\"}],\"homes\":[{\"latitude\":\"52.70590118\",\"longitude\":\"-0.27703028\"},{\"latitude\":\"52.14198724\",\"longitude\":\"-0.64274289\"},{\"latitude\":\"51.23826429\",\"longitude\":\"-0.94359512\"},{\"latitude\":\"51.45756508\",\"longitude\":\"-2.02257714\"},{\"latitude\":\"51.18619263\",\"longitude\":\"-1.12419993\"},{\"latitude\":\"51.37952969\",\"longitude\":\"-0.04702272\"},{\"latitude\":\"51.9539037\",\"longitude\":\"0.30955411\"},{\"latitude\":\"52.19172312\",\"longitude\":\"-2.31682087\"},{\"latitude\":\"51.55560229\",\"longitude\":\"0.18446219\"},{\"latitude\":\"51.59227371\",\"longitude\":\"-2.04253893\"},{\"latitude\":\"51.62554732\",\"longitude\":\"0.27630184\"},{\"latitude\":\"52.77793788\",\"longitude\":\"-1.19594326\"},{\"latitude\":\"54.76231286\",\"longitude\":\"-2.15259106\"},{\"latitude\":\"54.09946168\",\"longitude\":\"-3.04441542\"},{\"latitude\":\"54.12222696\",\"longitude\":\"-2.24433386\"},{\"latitude\":\"53.86197053\",\"longitude\":\"-1.59445142\"},{\"latitude\":\"54.98747681\",\"longitude\":\"-1.84413705\"},{\"latitude\":\"54.26699659\",\"longitude\":\"-2.37004153\"},{\"latitude\":\"54.36047368\",\"longitude\":\"-2.27091147\"},{\"latitude\":\"53.94346613\",\"longitude\":\"-1.46912767\"},{\"latitude\":\"53.79205954\",\"longitude\":\"-1.8458098\"},{\"latitude\":\"54.97706206\",\"longitude\":\"-2.36777296\"},{\"latitude\":\"54.58090769\",\"longitude\":\"-1.46180501\"},{\"latitude\":\"54.73205593\",\"longitude\":\"-2.24643121\"},{\"latitude\":\"54.4383307\",\"longitude\":\"-1.50939993\"},{\"latitude\":\"54.42164084\",\"longitude\":\"-2.0604822\"},{\"latitude\":\"54.46756311\",\"longitude\":\"-1.09038679\"},{\"latitude\":\"54.51814426\",\"longitude\":\"-2.01826612\"},{\"latitude\":\"54.02697483\",\"longitude\":\"-1.7763737\"},{\"latitude\":\"54.82286341\",\"longitude\":\"-1.24802256\"},{\"latitude\":\"54.76480865\",\"longitude\":\"-3.17767503\"},{\"latitude\":\"54.25271131\",\"longitude\":\"-1.56104494\"},{\"latitude\":\"53.89383572\",\"longitude\":\"-2.82198219\"},{\"latitude\":\"53.98134395\",\"longitude\":\"-3.1484841\"},{\"latitude\":\"54.35127041\",\"longitude\":\"-3.44403978\"},{\"latitude\":\"53.92669488\",\"longitude\":\"-1.30027827\"},{\"latitude\":\"54.26609108\",\"longitude\":\"-2.54279987\"},{\"latitude\":\"54.82583079\",\"longitude\":\"-3.00937635\"},{\"latitude\":\"54.43794348\",\"longitude\":\"-1.20316182\"},{\"latitude\":\"53.79637861\",\"longitude\":\"-1.56080153\"},{\"latitude\":\"53.95115316\",\"longitude\":\"-1.34681629\"},{\"latitude\":\"54.79243295\",\"longitude\":\"-2.93634982\"},{\"latitude\":\"54.03440424\",\"longitude\":\"-1.44575508\"},{\"latitude\":\"54.64058088\",\"longitude\":\"-2.04931387\"},{\"latitude\":\"54.83128889\",\"longitude\":\"-1.39748235\"},{\"latitude\":\"54.10674969\",\"longitude\":\"-1.100992\"},{\"latitude\":\"54.67974332\",\"longitude\":\"-3.14168241\"},{\"latitude\":\"54.58858916\",\"longitude\":\"-1.94382326\"},{\"latitude\":\"54.55186227\",\"longitude\":\"-3.31521355\"},{\"latitude\":\"54.54741555\",\"longitude\":\"-3.12293662\"},{\"latitude\":\"54.6686201\",\"longitude\":\"-2.27664552\"},{\"latitude\":\"54.10670872\",\"longitude\":\"-2.66905384\"},{\"latitude\":\"54.18541752\",\"longitude\":\"-1.40896384\"},{\"latitude\":\"53.85370191\",\"longitude\":\"-2.11415901\"},{\"latitude\":\"54.4612711\",\"longitude\":\"-2.38838359\"},{\"latitude\":\"54.35932231\",\"longitude\":\"-2.95671983\"},{\"latitude\":\"54.83224135\",\"longitude\":\"-2.02107972\"},{\"latitude\":\"53.94492481\",\"longitude\":\"-2.615292\"},{\"latitude\":\"54.43641403\",\"longitude\":\"-2.00975828\"},{\"latitude\":\"54.66564463\",\"longitude\":\"-3.2353939\"},{\"latitude\":\"54.09085574\",\"longitude\":\"-2.44940968\"},{\"latitude\":\"54.87073428\",\"longitude\":\"-2.17959051\"},{\"latitude\":\"54.69873406\",\"longitude\":\"-2.30577751\"},{\"latitude\":\"53.80812796\",\"longitude\":\"-2.42324984\"},{\"latitude\":\"54.2052736\",\"longitude\":\"-2.94371337\"},{\"latitude\":\"54.5099245\",\"longitude\":\"-1.67772737\"},{\"latitude\":\"54.21702032\",\"longitude\":\"-1.58210741\"},{\"latitude\":\"53.83761409\",\"longitude\":\"-2.64762429\"},{\"latitude\":\"54.85281874\",\"longitude\":\"-3.03650065\"},{\"latitude\":\"54.03425571\",\"longitude\":\"-1.38772019\"},{\"latitude\":\"54.80690007\",\"longitude\":\"-2.02233956\"},{\"latitude\":\"54.97552141\",\"longitude\":\"-2.91809731\"},{\"latitude\":\"54.38200429\",\"longitude\":\"-3.22300142\"},{\"latitude\":\"54.16414387\",\"longitude\":\"-1.86395656\"},{\"latitude\":\"55.00397692\",\"longitude\":\"-2.62545621\"},{\"latitude\":\"54.64125151\",\"longitude\":\"-1.28861772\"},{\"latitude\":\"54.27513151\",\"longitude\":\"-1.41867207\"},{\"latitude\":\"54.71048281\",\"longitude\":\"-1.99843531\"},{\"latitude\":\"54.4656675\",\"longitude\":\"-1.25563832\"},{\"latitude\":\"54.44694146\",\"longitude\":\"-2.15383409\"},{\"latitude\":\"54.07966129\",\"longitude\":\"-1.9863118\"},{\"latitude\":\"54.10907676\",\"longitude\":\"-2.72372686\"},{\"latitude\":\"54.89640738\",\"longitude\":\"-1.52863233\"},{\"latitude\":\"54.48311436\",\"longitude\":\"-1.5552409\"},{\"latitude\":\"54.05465039\",\"longitude\":\"-1.68308187\"},{\"latitude\":\"54.99433855\",\"longitude\":\"-1.67103595\"},{\"latitude\":\"54.32606915\",\"longitude\":\"-3.24585147\"},{\"latitude\":\"54.56674983\",\"longitude\":\"-2.13179062\"},{\"latitude\":\"54.09072443\",\"longitude\":\"-2.61905477\"},{\"latitude\":\"54.77965168\",\"longitude\":\"-2.86847254\"},{\"latitude\":\"54.21141905\",\"longitude\":\"-2.95263331\"},{\"latitude\":\"54.12346515\",\"longitude\":\"-1.79647177\"},{\"latitude\":\"53.9663717\",\"longitude\":\"-1.78940891\"},{\"latitude\":\"54.91988103\",\"longitude\":\"-2.90127607\"},{\"latitude\":\"54.22177629\",\"longitude\":\"-2.57537266\"},{\"latitude\":\"53.85946608\",\"longitude\":\"-1.49289878\"},{\"latitude\":\"53.76427117\",\"longitude\":\"-1.66260645\"},{\"latitude\":\"54.78290625\",\"longitude\":\"-2.78069762\"},{\"latitude\":\"54.14876361\",\"longitude\":\"-2.31001972\"},{\"latitude\":\"53.93364236\",\"longitude\":\"-2.10824614\"},{\"latitude\":\"54.55065082\",\"longitude\":\"-1.59030189\"},{\"latitude\":\"54.09878178\",\"longitude\":\"-1.52917452\"},{\"latitude\":\"54.24859883\",\"longitude\":\"-2.77178922\"},{\"latitude\":\"54.30957345\",\"longitude\":\"-2.55656031\"},{\"latitude\":\"53.76165641\",\"longitude\":\"-2.34224788\"},{\"latitude\":\"54.125876\",\"longitude\":\"-3.11143121\"},{\"latitude\":\"54.26873702\",\"longitude\":\"-2.27733778\"},{\"latitude\":\"54.95224126\",\"longitude\":\"-2.56886115\"},{\"latitude\":\"54.26765486\",\"longitude\":\"-2.95572095\"},{\"latitude\":\"54.44501463\",\"longitude\":\"-1.94358789\"},{\"latitude\":\"53.9357763\",\"longitude\":\"-2.55504595\"},{\"latitude\":\"53.97826822\",\"longitude\":\"-2.92837504\"},{\"latitude\":\"55.83542346\",\"longitude\":\"-3.61806485\"},{\"latitude\":\"56.89668838\",\"longitude\":\"-3.98379141\"},{\"latitude\":\"55.97276035\",\"longitude\":\"-4.6166741\"},{\"latitude\":\"56.4752055\",\"longitude\":\"-3.17707101\"},{\"latitude\":\"56.92771404\",\"longitude\":\"-3.8127502\"},{\"latitude\":\"56.57422428\",\"longitude\":\"-4.97943526\"},{\"latitude\":\"56.50828448\",\"longitude\":\"-3.19584417\"},{\"latitude\":\"56.35339124\",\"longitude\":\"-3.62265812\"},{\"latitude\":\"56.74131097\",\"longitude\":\"-3.46385056\"},{\"latitude\":\"56.97268423\",\"longitude\":\"-4.58944037\"},{\"latitude\":\"55.79796207\",\"longitude\":\"-3.97966737\"},{\"latitude\":\"56.7900099\",\"longitude\":\"-4.82554583\"},{\"latitude\":\"56.86795998\",\"longitude\":\"-2.94785005\"},{\"latitude\":\"56.1166064\",\"longitude\":\"-4.62773598\"},{\"latitude\":\"56.64588067\",\"longitude\":\"-4.51482165\"},{\"latitude\":\"56.30132454\",\"longitude\":\"-5.01709811\"},{\"latitude\":\"56.45715865\",\"longitude\":\"-3.56434037\"},{\"latitude\":\"56.05930699\",\"longitude\":\"-4.36012439\"},{\"latitude\":\"55.97434819\",\"longitude\":\"-3.78695006\"},{\"latitude\":\"57.1842394\",\"longitude\":\"-3.41812831\"},{\"latitude\":\"56.50706269\",\"longitude\":\"-5.03925096\"},{\"latitude\":\"56.34056074\",\"longitude\":\"-2.70793406\"},{\"latitude\":\"56.26721209\",\"longitude\":\"-3.74611339\"},{\"latitude\":\"56.4060058\",\"longitude\":\"-3.50556092\"},{\"latitude\":\"56.62439967\",\"longitude\":\"-4.47288424\"},{\"latitude\":\"56.07519331\",\"longitude\":\"-2.96772049\"},{\"latitude\":\"56.28239052\",\"longitude\":\"-4.07939183\"},{\"latitude\":\"57.01933426\",\"longitude\":\"-4.54600401\"},{\"latitude\":\"56.81310027\",\"longitude\":\"-3.99088276\"},{\"latitude\":\"56.49027694\",\"longitude\":\"-2.87060689\"},{\"latitude\":\"50.74002677\",\"longitude\":\"-4.69882123\"},{\"latitude\":\"51.12792404\",\"longitude\":\"-3.51639952\"},{\"latitude\":\"51.81947776\",\"longitude\":\"-2.60751655\"},{\"latitude\":\"52.29646543\",\"longitude\":\"-3.11300116\"},{\"latitude\":\"51.11392037\",\"longitude\":\"-2.29485739\"},{\"latitude\":\"50.68645059\",\"longitude\":\"-2.21610801\"},{\"latitude\":\"51.67906696\",\"longitude\":\"-4.78616293\"},{\"latitude\":\"51.7931377\",\"longitude\":\"-3.44340695\"},{\"latitude\":\"51.3011024\",\"longitude\":\"-2.8554516\"},{\"latitude\":\"51.32691057\",\"longitude\":\"-3.00792456\"},{\"latitude\":\"51.8100017\",\"longitude\":\"-3.5777956\"},{\"latitude\":\"50.89134112\",\"longitude\":\"-4.29562926\"},{\"latitude\":\"51.62942634\",\"longitude\":\"-4.72938131\"},{\"latitude\":\"52.12789439\",\"longitude\":\"-2.48284566\"},{\"latitude\":\"50.67332902\",\"longitude\":\"-4.62612438\"},{\"latitude\":\"50.86180274\",\"longitude\":\"-4.11589261\"},{\"latitude\":\"50.29752746\",\"longitude\":\"-3.67524036\"},{\"latitude\":\"50.94554558\",\"longitude\":\"-5.14082044\"},{\"latitude\":\"52.39877918\",\"longitude\":\"-3.09173385\"},{\"latitude\":\"51.25853966\",\"longitude\":\"-5.30252203\"},{\"latitude\":\"51.5966211\",\"longitude\":\"-2.42598034\"},{\"latitude\":\"50.60711617\",\"longitude\":\"-4.46375733\"},{\"latitude\":\"52.16370577\",\"longitude\":\"-3.63134752\"},{\"latitude\":\"50.72412875\",\"longitude\":\"-3.41756291\"},{\"latitude\":\"51.20476581\",\"longitude\":\"-5.23146459\"},{\"latitude\":\"50.86481107\",\"longitude\":\"-3.47535476\"},{\"latitude\":\"51.02929141\",\"longitude\":\"-3.54312164\"},{\"latitude\":\"50.78811253\",\"longitude\":\"-3.8025846\"},{\"latitude\":\"52.52717539\",\"longitude\":\"-4.38988302\"},{\"latitude\":\"51.99339816\",\"longitude\":\"-2.43279934\"},{\"latitude\":\"51.65227148\",\"longitude\":\"-2.55307266\"},{\"latitude\":\"50.41033467\",\"longitude\":\"-3.73664696\"},{\"latitude\":\"50.64974128\",\"longitude\":\"-3.02273384\"},{\"latitude\":\"50.89186029\",\"longitude\":\"-4.6071819\"},{\"latitude\":\"51.17743651\",\"longitude\":\"-1.50307788\"},{\"latitude\":\"51.99650202\",\"longitude\":\"-2.7460413\"},{\"latitude\":\"50.77046005\",\"longitude\":\"-2.93033352\"},{\"latitude\":\"51.28288791\",\"longitude\":\"-3.34203143\"},{\"latitude\":\"51.60447587\",\"longitude\":\"-4.64917814\"},{\"latitude\":\"50.62694022\",\"longitude\":\"-3.2736577\"},{\"latitude\":\"52.14404138\",\"longitude\":\"-3.05838875\"},{\"latitude\":\"50.93464282\",\"longitude\":\"-1.58557305\"},{\"latitude\":\"50.46609653\",\"longitude\":\"-4.66201156\"},{\"latitude\":\"51.38086519\",\"longitude\":\"-3.93346953\"},{\"latitude\":\"51.25210672\",\"longitude\":\"-2.35625798\"},{\"latitude\":\"52.03416\",\"longitude\":\"-5.26382936\"},{\"latitude\":\"51.63027175\",\"longitude\":\"-4.43325967\"},{\"latitude\":\"52.19041388\",\"longitude\":\"-4.11304693\"},{\"latitude\":\"51.86796629\",\"longitude\":\"-5.35296158\"},{\"latitude\":\"51.57395752\",\"longitude\":\"-5.06966414\"},{\"latitude\":\"51.23925496\",\"longitude\":\"-3.34254409\"},{\"latitude\":\"51.07915984\",\"longitude\":\"-5.41836762\"},{\"latitude\":\"50.47698206\",\"longitude\":\"-2.45233081\"},{\"latitude\":\"51.27183925\",\"longitude\":\"-3.04589853\"},{\"latitude\":\"51.91904526\",\"longitude\":\"-2.08839373\"},{\"latitude\":\"52.69900714\",\"longitude\":\"-4.01786216\"},{\"latitude\":\"51.46293089\",\"longitude\":\"-4.88087481\"},{\"latitude\":\"50.70092035\",\"longitude\":\"-4.4116084\"},{\"latitude\":\"52.64548338\",\"longitude\":\"-3.13568287\"},{\"latitude\":\"51.74321777\",\"longitude\":\"-3.29689936\"},{\"latitude\":\"50.3399111\",\"longitude\":\"-3.57950955\"},{\"latitude\":\"51.82784269\",\"longitude\":\"-4.3394999\"},{\"latitude\":\"51.07814061\",\"longitude\":\"-4.83413633\"},{\"latitude\":\"50.1659902\",\"longitude\":\"-3.68531521\"},{\"latitude\":\"51.44356839\",\"longitude\":\"-3.89519047\"},{\"latitude\":\"51.67461836\",\"longitude\":\"-4.05282516\"},{\"latitude\":\"50.90896044\",\"longitude\":\"-1.61062935\"},{\"latitude\":\"51.43013081\",\"longitude\":\"-3.09707864\"},{\"latitude\":\"51.12714875\",\"longitude\":\"-1.87237427\"},{\"latitude\":\"52.00133427\",\"longitude\":\"-3.54804925\"},{\"latitude\":\"52.75104766\",\"longitude\":\"0.98179087\"},{\"latitude\":\"52.55761772\",\"longitude\":\"1.46483538\"},{\"latitude\":\"51.94726064\",\"longitude\":\"-0.5633538\"},{\"latitude\":\"51.60494191\",\"longitude\":\"-0.37323615\"},{\"latitude\":\"52.05718191\",\"longitude\":\"0.20962927\"},{\"latitude\":\"52.15189299\",\"longitude\":\"1.26959061\"},{\"latitude\":\"52.57590383\",\"longitude\":\"0.95286965\"},{\"latitude\":\"52.91077539\",\"longitude\":\"0.01992932\"},{\"latitude\":\"53.12067245\",\"longitude\":\"0.25128446\"},{\"latitude\":\"51.43573098\",\"longitude\":\"-0.06992911\"},{\"latitude\":\"52.56723653\",\"longitude\":\"-0.53509823\"},{\"latitude\":\"51.89702765\",\"longitude\":\"1.43850263\"},{\"latitude\":\"52.88647752\",\"longitude\":\"1.49317284\"},{\"latitude\":\"51.89030332\",\"longitude\":\"0.64504673\"},{\"latitude\":\"51.67860649\",\"longitude\":\"-0.48406838\"},{\"latitude\":\"52.25877905\",\"longitude\":\"1.55463918\"},{\"latitude\":\"52.94592521\",\"longitude\":\"1.66671464\"},{\"latitude\":\"51.51703397\",\"longitude\":\"0.60123628\"},{\"latitude\":\"52.86032656\",\"longitude\":\"0.19209637\"},{\"latitude\":\"52.13483791\",\"longitude\":\"0.61076177\"},{\"latitude\":\"52.37527837\",\"longitude\":\"0.8363054\"},{\"latitude\":\"52.7498671\",\"longitude\":\"1.07203725\"},{\"latitude\":\"51.62682993\",\"longitude\":\"1.77890324\"},{\"latitude\":\"51.35766931\",\"longitude\":\"0.18108719\"},{\"latitude\":\"51.66018053\",\"longitude\":\"0.83159571\"},{\"latitude\":\"52.28355787\",\"longitude\":\"2.07320992\"},{\"latitude\":\"52.12250285\",\"longitude\":\"0.35196094\"},{\"latitude\":\"52.056673\",\"longitude\":\"1.34034102\"},{\"latitude\":\"51.95424176\",\"longitude\":\"0.16016661\"},{\"latitude\":\"52.19808305\",\"longitude\":\"1.72284793\"},{\"latitude\":\"52.36487946\",\"longitude\":\"-0.28883578\"},{\"latitude\":\"52.59318739\",\"longitude\":\"-0.00734457\"},{\"latitude\":\"51.95478792\",\"longitude\":\"-0.43532229\"},{\"latitude\":\"52.94077143\",\"longitude\":\"-0.37175872\"},{\"latitude\":\"52.77412187\",\"longitude\":\"0.8437271\"},{\"latitude\":\"51.94256895\",\"longitude\":\"2.13349626\"},{\"latitude\":\"52.03601603\",\"longitude\":\"1.75786473\"},{\"latitude\":\"51.54147873\",\"longitude\":\"0.13684994\"},{\"latitude\":\"51.3526128\",\"longitude\":\"0.39737719\"},{\"latitude\":\"52.14200014\",\"longitude\":\"1.38593472\"},{\"latitude\":\"52.97999789\",\"longitude\":\"1.3710141\"},{\"latitude\":\"51.29805656\",\"longitude\":\"1.47479207\"},{\"latitude\":\"51.49226768\",\"longitude\":\"1.11636699\"},{\"latitude\":\"51.78080954\",\"longitude\":\"1.63175053\"},{\"latitude\":\"51.71484201\",\"longitude\":\"0.60066747\"},{\"latitude\":\"51.31724754\",\"longitude\":\"0.96424216\"},{\"latitude\":\"51.80615732\",\"longitude\":\"1.12808147\"},{\"latitude\":\"52.03031081\",\"longitude\":\"1.40260478\"},{\"latitude\":\"52.49801043\",\"longitude\":\"0.85655377\"},{\"latitude\":\"52.98315389\",\"longitude\":\"0.86364907\"},{\"latitude\":\"52.51120178\",\"longitude\":\"0.05126101\"},{\"latitude\":\"53.16114546\",\"longitude\":\"1.02845474\"},{\"latitude\":\"52.62018906\",\"longitude\":\"1.72229423\"},{\"latitude\":\"52.51761359\",\"longitude\":\"-0.59345321\"},{\"latitude\":\"51.58161921\",\"longitude\":\"0.16058089\"},{\"latitude\":\"52.02112877\",\"longitude\":\"2.37218882\"},{\"latitude\":\"52.22764808\",\"longitude\":\"0.70408644\"},{\"latitude\":\"52.44106599\",\"longitude\":\"0.51942081\"},{\"latitude\":\"52.20541482\",\"longitude\":\"-0.54179658\"},{\"latitude\":\"52.72058824\",\"longitude\":\"1.09607733\"},{\"latitude\":\"52.43590998\",\"longitude\":\"1.66511675\"},{\"latitude\":\"51.25409871\",\"longitude\":\"0.63557456\"},{\"latitude\":\"51.60969988\",\"longitude\":\"0.00668441\"},{\"latitude\":\"51.75141856\",\"longitude\":\"1.17238249\"},{\"latitude\":\"51.61879459\",\"longitude\":\"2.0577057\"},{\"latitude\":\"52.3709426\",\"longitude\":\"-0.63868052\"},{\"latitude\":\"53.05674397\",\"longitude\":\"0.54545787\"},{\"latitude\":\"51.59539206\",\"longitude\":\"0.38279204\"},{\"latitude\":\"52.61667813\",\"longitude\":\"0.13587709\"},{\"latitude\":\"52.95111848\",\"longitude\":\"0.44061441\"}]}");

/***/ }),

/***/ "@apollo/federation":
/*!*************************************!*\
  !*** external "@apollo/federation" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@apollo/federation");

/***/ }),

/***/ "@graphql-modules/core":
/*!****************************************!*\
  !*** external "@graphql-modules/core" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@graphql-modules/core");

/***/ }),

/***/ "apollo-server":
/*!********************************!*\
  !*** external "apollo-server" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("apollo-server");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "geolib":
/*!*************************!*\
  !*** external "geolib" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("geolib");

/***/ })

/******/ });
//# sourceMappingURL=serverBundle.js.map