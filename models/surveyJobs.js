"use strict";
const mongoose = require("mongoose");

const SurveyJobchema = new mongoose.Schema({
  name: { type: String, required: true }
});

const SurveyJobModel = mongoose.model(
  "surveyJobs",
  SurveyJobchema,
  "surveyJobs"
);

const toObjectId = strId => mongoose.Types.ObjectId(strId);

const getAll = async params => SurveyJobModel.find(params);

const getById = async jobRoleId => SurveyJobModel.findById(jobRoleId);

const getByIds = async jobRoleIds => {
  const ids = jobRoleIds.map(id => toObjectId(id));
  return SurveyJobModel.find({ _id: { $in: ids } });
};

const getByTextSearch = async text =>
  SurveyJobModel.find({ name: new RegExp(text, "gi") });

module.exports = {
  getById,
  getByIds,
  getAll,
  getByTextSearch
};
