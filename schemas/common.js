"use strict";
const { gql } = require("apollo-server");
const { GraphQLModule } = require("@graphql-modules/core");
const { PubSub } = require("apollo-server");
const { CarSimulator } = require("../utils/CarSimulator");

const pubsub = new PubSub();

const CAR_SIMULATOR_STREAM = "CAR_SIMULATOR_STREAM";
const carSimulator = new CarSimulator(1, pubsub, CAR_SIMULATOR_STREAM);

carSimulator.startSimulation(100, 500, 20000);

const typeDefs = gql`
  type Message {
    id: ID!
    user: String!
    content: String!
  }

  type EmissionData {
    pm1: Int
    pm2: Int
    pm10: Int
  }

  type Car {
    uuid: ID!
    latitude: Float!
    longitude: Float!
    emissionData: EmissionData
  }

  type Query {
    messages: [Message!]
  }

  type Mutation {
    stopStream: Boolean
  }

  type Subscription {
    streamCarData: [Car]
  }
`;

const resolvers = {
  Query: {
    messages: () => null
  },
  Mutation: {
    stopStream: (parent, {}) => {
      // const id = messages.length;
      // messages.push({
      //   id, user, content
      // })
      carSimulator.stopSimulation();
      // pubsub.publish(CAR_SIMULATOR_STREAM, {streamMessages: messages});
      // subscribers.forEach((fn) => fn())
      return true;
    }
  },
  Subscription: {
    streamCarData: {
      subscribe: () => {
        // onMessagesUpdates(() => pubsub.publish(POST_ADDED, { messages }));
        // setTimeout(() => pubsub.publish(CAR_SIMULATOR_STREAM, { messages }), 0);
        return pubsub.asyncIterator([CAR_SIMULATOR_STREAM]);
      }
    }
  }
};

module.exports = new GraphQLModule({
  typeDefs,
  resolvers,
  context: session => session
});
