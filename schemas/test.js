"use strict";
const { gql } = require("apollo-server");
const { GraphQLModule } = require("@graphql-modules/core");

const typeDefs = gql`

  scalar Date

  extend type Industry @key(fields: "id") {
    id: ID! @external
  }

  extend type Country @key(fields: "id") {
    id: ID! @external
  }

  type SurveyTextContent {
    type: String
    content: String
  }

  input SurveyKeyValuesInput {
    key: String
    values: [String]
  }

  input SurveyTextContentInput {
    type: String
    content: String
  }
`;

const resolvers = {};

module.exports = new GraphQLModule({
  typeDefs,
  resolvers,
  context: session => session
});
