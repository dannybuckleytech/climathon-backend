"use strict";
const { ApolloServer } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");
const { GraphQLModule } = require("@graphql-modules/core");

// const mongoose = require("mongoose");
// const aws = require("aws-sdk");
const fs = require("fs");
const config = require("./config");


// ──────────────────────── COORDINATE CONVERTER ───────────────────────────────────────────────────────

// const {starts} = require("./utils/coordinateConverter/coordinateConverter")

// // starts()





// const axios = require("axios");
// const dataLoader = require("./dataLoader/loader");

// const cronTasks = require("./cron/index");
// const surveyResultsLoader = require("./workers/submissionResults/loader");

// const directives = require("./directives/index");

// const startWorkerProcess = () => {
//   //run survey results process every 5s
//   setInterval(surveyResultsLoader.processSubmissions, 5000);
// };

/**
 * Merge all subschemas into a single main one.
 */
const { schemaModules } = require("./schemas");

const mergeSchemas = () =>
  new GraphQLModule({
    imports: schemaModules
  });

// const initialiseAWS = credentials => aws.config.update(credentials);

/**
 * Retrieves the json secrets file
 * @param {*} fileName
 */
const getAppSecrets = secretsFileName => {
  let content = null;
  try {
    let filePath = config.DOCKER_MODE
      ? `/run/secrets/${secretsFileName}`
      : secretsFileName;
    console.log("secret file path", filePath);
    let strContent = fs.readFileSync(filePath, "utf-8");
    content = JSON.parse(strContent);
  } catch (error) {
    console.error(new Date(), "Unable to access secrets", error);
  }
  return content;
};

// const registryUrl = `http://${config.SERVICE_PROJECT}_${config.REGISTRY_SERVICE}_${config.ENV}:${config.REGISTRY_PORT}/register?type=graphql`;
// const serviceUrl = `http://${config.SERVICE_PROJECT}_${config.SERVICE_NAME}_${config.ENV}:${config.PORT}/graphql`;

const secrets = getAppSecrets(config.SECRETS_FILE);

// The split function takes three parameters:
//
// * A function that's called for each operation to execute
// * The Link to use for an operation if the function returns a "truthy" value
// * The Link to use for an operation if the function returns a "falsy" value

//////////////////////////////////////////

const { typeDefs, resolvers } = mergeSchemas();

const schema = buildFederatedSchema([{ typeDefs, resolvers }]);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req, connection }) => {
    if (connection) {
      // check connection for metadata
      return connection.context;
    } else {
      // check from req
      const token = req.headers.authorization || "";

      return { token };
    }
  }
});

server.listen({ port: config.PORT }).then(({ url, subscriptionsUrl }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});


export default server.httpServer;
