const convertedCoordinates = require("./coordinateConverter/lib/convertedCoordinates.json");
const geolib = require("geolib");
const axios = require("axios");

const getHome = () => {
  const { homes } = convertedCoordinates;
  return homes[Math.floor(Math.random() * homes.length)];
};

const getTargetLocation = home => {
  const { cities } = convertedCoordinates;
  const distancedMappedCities = cities.map(city => {
    const { cityName, options } = city;
    const selectedLocationCoordinates =
      options[Math.floor(Math.random() * options.length)];
    const calculatedDistance = geolib.getDistance(
      {
        latitude: selectedLocationCoordinates.latitude,
        longitude: selectedLocationCoordinates.longitude
      },
      { latitude: home.latitude, longitude: home.longitude }
    );
    return {
      cityName,
      longitude: selectedLocationCoordinates.longitude,
      latitude: selectedLocationCoordinates.latitude,
      distance: calculatedDistance
    };
  });
  return distancedMappedCities.sort(
    ({ distance: distanceA }, { distance: distanceB }) => distanceA - distanceB
  )[0];
};

const flattenRoute = routeData => {
  const flattenedRoute = [];
  routeData.routes[0].legs[0].steps.forEach((step, stepIndex) =>
    step.intersections.forEach((intersection, intersectionIndex) => {
      const combinedIndex = stepIndex + intersectionIndex / 10;
      flattenedRoute.push({
        index: combinedIndex,
        longitude: intersection.location[0],
        latitude: intersection.location[1],
        uuid: Math.floor(Math.random() * 10000000)
      });
    })
  );
  return flattenedRoute;
};

const orderFlattenedRoute = flattenedRoute => {
  return flattenedRoute.sort(({ indexA }, { indexB }) => indexA - indexB);
};

const getRoute = async (home, targetLocation) => {
  try {
    const res = await axios.get(
      `http://router.project-osrm.org/route/v1/driving/${home.longitude},${home.latitude};${targetLocation.longitude},${targetLocation.latitude}?overview=false&steps=true`
    );
    if (res.data) {
      return { rawRoute: res.data, flattenedRoute: flattenRoute(res.data) };
    }
    return false;
  } catch (err) {
    console.log("ERROR", err);
  }
  return false;
};

const moveDistance = ({
  tickDistance,
  currentLatitude,
  currentLongitude,
  flattenedRoute,
  currentRouteStepUuid,
  goingToTarget,
  stoppedTraveling,
  carUuid
}) => {

  const roundedCoordinate = coor => Math.round(coor * 100000000) / 100000000;
  const localGoingToTarget = goingToTarget;
  const localCurrent = {
    latitude: roundedCoordinate(parseFloat(currentLatitude)),
    longitude: roundedCoordinate(parseFloat(currentLongitude))
  };

  if (stoppedTraveling) {
    console.log(`CAR ${carUuid} STOPPED TRAVELING`)

    return {
      latitude: roundedCoordinate(localCurrent.latitude),
      longitude: roundedCoordinate(localCurrent.longitude),
      goingToTarget: localGoingToTarget,
      stoppedTraveling: true,
      currentRouteStepUuid: currentRouteStepUuid
    };
  } else {
    console.log(`CAR ${carUuid} DRIVING TO ${goingToTarget ? "WORK": "HOME"}`)
  }

  const calculateNextStep = scopeCurrentRouteStepUuid => {
    const currentRouteStepIndex = flattenedRoute.findIndex(
      step => step.uuid === scopeCurrentRouteStepUuid
    );

    return localGoingToTarget
      ? flattenedRoute[currentRouteStepIndex + 1]
      : flattenedRoute[currentRouteStepIndex - 1];
  };

  let nextStep = calculateNextStep(currentRouteStepUuid);

  if (!nextStep) {
    debugger;
    console.log("FAIL SAFE - REACHED END OF ROUTE");
    return {
      latitude: roundedCoordinate(localCurrent.latitude),
      longitude: roundedCoordinate(localCurrent.longitude),
      goingToTarget: !localGoingToTarget,
      stoppedTraveling: true,
      currentRouteStepUuid: currentRouteStepUuid
    };
  }

  //calculate move
  let stepComplete = false;
  const remainingMovement = {
    latitude: tickDistance,
    longitude: tickDistance
  };

  while (!stepComplete) {
  // console.log('---LOCAL CURRENT LAT', localCurrent.latitude)
  // console.log('---LOCAL CURRENT LONG', localCurrent.longitude)
    //Move to next step if reached current step
    if (
      roundedCoordinate(localCurrent.latitude) === roundedCoordinate(nextStep.latitude) &&
      roundedCoordinate(localCurrent.longitude) === roundedCoordinate(nextStep.longitude)
    ) {

      let savedStep = nextStep;
      nextStep = calculateNextStep(nextStep.uuid);

      if (!nextStep) {
        console.log("CAR REACHED END OF ROUTE, REVERSING, STOPPING", {
          latitude: roundedCoordinate(savedStep.latitude),
          longitude: roundedCoordinate(savedStep.longitude),
          goingToTarget: !goingToTarget,
          stoppedTraveling: true,
          currentRouteStepUuid: savedStep.uuid
        });

        return {
          latitude: roundedCoordinate(savedStep.latitude),
          longitude: roundedCoordinate(savedStep.longitude),
          goingToTarget: !localGoingToTarget,
          stoppedTraveling: true,
          currentRouteStepUuid: savedStep.uuid
        };
      } else {
        // console.log(
        //   "NEW STEP REACHED",
        //   nextStep.index,
        //   flattenedRoute[flattenedRoute.length - 1].index
        // );
      }
    }
    const requiredMovement = {
      latitude: Math.abs(
        parseFloat(localCurrent.latitude) - parseFloat(nextStep.latitude)
      ),
      longitude: Math.abs(
        parseFloat(localCurrent.longitude) - parseFloat(nextStep.longitude)
      )
    };

    const performCalc = type => {
      if (remainingMovement[type] >= Math.abs(requiredMovement[type])) {
        //Able to reach target within this cycle
        localCurrent[type] = nextStep[type];
        remainingMovement[type] -= requiredMovement[type];
      } else {
        // otherwise move in correct direction as much as possible
        if (localCurrent[type] >= nextStep[type]) {
          localCurrent[type] -= remainingMovement[type];
        } else {
          localCurrent[type] += remainingMovement[type];
        }
        remainingMovement[type] = 0;
      }
    };

    performCalc("latitude");
    performCalc("longitude");

    //check if at target
    const targetLocation = goingToTarget
      ? flattenedRoute[flattenedRoute.length - 1]
      : flattenedRoute[0];
    if (
      roundedCoordinate(localCurrent.latitude) ===
        roundedCoordinate(targetLocation.latitude) &&
      roundedCoordinate(localCurrent.longitude) ===
        roundedCoordinate(targetLocation.longitude)
    ) {

      return {
        latitude: roundedCoordinate(targetLocation.latitude),
        longitude: roundedCoordinate(targetLocation.longitude),
        goingToTarget: !localGoingToTarget,
        stoppedTraveling: true,
        currentRouteStepUuid: targetLocation.uuid
      };
    }
    // console.log('CYCLE COMPLETE')
    // console.log('LAT REMAINING', remainingMovement.latitude)
    // console.log('LANG REMAINING', remainingMovement.longitude)
    stepComplete = remainingMovement.latitude === 0 || remainingMovement.longitude === 0;
  }
  // console.log("COMPLETED MOVE", {
  //   latitude: roundedCoordinate(localCurrent.latitude),
  //   longitude: roundedCoordinate(localCurrent.longitude),
  //   goingToTarget: localGoingToTarget,
  //   stoppedTraveling: false,
  //   currentRouteStepUuid: nextStep.uuid
  // });
  return {
    latitude: roundedCoordinate(localCurrent.latitude),
    longitude: roundedCoordinate(localCurrent.longitude),
    goingToTarget: localGoingToTarget,
    stoppedTraveling: false,
    currentRouteStepUuid: nextStep.uuid
  };
};

const calculateEmissionValues = () => {
  return {
    pm1: Math.floor(Math.random() * 400000000) + 600000000,
    pm2: Math.floor(Math.random() * 300000000) + 500000000,
    pm10: Math.floor(Math.random() * 100000000) + 300000000
  }
}

class Car {
  constructor(tickDistance = 0.5) {
    this.uuid = Math.floor(Math.random() * 10000000);
    this.tickDistance = tickDistance;
    this.deactivate = false;
    this.emissionData = calculateEmissionValues()
  }

  async initialise() {
    this.home = getHome();
    this.getTargetLocation = getTargetLocation(this.home);
    this.route = await getRoute(this.home, this.getTargetLocation);
    this.latitude = this.home.latitude;
    this.longitude = this.home.longitude;
    this.currentRouteStepUuid = this.route ? this.route.flattenedRoute[0].uuid : null;
    this.goingToTarget = true;
    this.stoppedTraveling = false;
  }

  get getFlattenedRoute() {
    if (this.route) {
      return orderFlattenedRoute(this.route.flattenedRoute);
    }
    return false;
  }

  toggleTravelingStatus() {
    this.stoppedTraveling = !this.stoppedTraveling
  }

  newTick() {
    this.emissionData = calculateEmissionValues()
    if (this.route) {
      const {
        latitude,
        longitude,
        goingToTarget,
        stoppedTraveling,
        currentRouteStepUuid
      } = moveDistance({
        tickDistance: this.tickDistance,
        currentLatitude: this.latitude,
        currentLongitude: this.longitude,
        flattenedRoute: this.getFlattenedRoute,
        currentRouteStepUuid: this.currentRouteStepUuid,
        goingToTarget: this.goingToTarget,
        stoppedTraveling: this.stoppedTraveling,
        carUuid: this.uuid
      });
      this.latitude = latitude;
      this.longitude = longitude;
      this.goingToTarget = goingToTarget;
      this.stoppedTraveling = stoppedTraveling;
      this.currentRouteStepUuid = currentRouteStepUuid;
      return true;
    }
    this.deactivate = true;
    return false;
  }
}

module.exports = {
  Car
};
