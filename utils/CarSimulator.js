const {Car} = require("./Car")
class CarSimulator {
  constructor(initialAmount, pubsub, broadcastFlag){
    this.amountOfCars = initialAmount
    this.pubsub = pubsub
    this.broadcastFlag = broadcastFlag
    this.active = false
    this.data = {number: 1}
    this.interval = null
    this.cars = []
    this.timeProcessed = 0
  }

  async startSimulation (amountOfCars = this.amountOfCars, frequency = 500, dayTransitionCycle = 5000){
    await this.createCars(amountOfCars)
    this.interval = setInterval(()=> {
      this.active = true
      this.cars.forEach((car) => car.newTick())
      this.timeProcessed += frequency
      if(this.timeProcessed % dayTransitionCycle === 0){
        console.log('TRANSITIONING DAY CYCLE')
        this.cars.forEach((car) => car.toggleTravelingStatus())
      }
      this.pubsub.publish(this.broadcastFlag, {streamCarData: this.cars});
    }, frequency)
  }

  async createCars (amount = 1){
    let amountLeft = amount
    while(amountLeft !== 0){
      const car = new Car(0.05)
      await car.initialise()
      this.cars.push(car)
      amountLeft -= 1
      console.log(amountLeft)
    }
  }

  clearCars(){
    this.cars = []
  }

  stopSimulation(){
    clearInterval(this.interval)
    this.clearCars()
    this.active = false
  }

}


module.exports = {
  CarSimulator
}