const {unconvertedCoordinates} = require("./lib/unconvertedCoordinates")
const fs = require('fs');

const starts = () => {
const {homes, cities} = unconvertedCoordinates
const lineSelectorRegex = /(.*)\//g
const singularCoordinates = {
  homes: homes.match(lineSelectorRegex),
  birmingham: cities.birmingham.match(lineSelectorRegex),
  sheffield: cities.sheffield.match(lineSelectorRegex),
  leeds: cities.leeds.match(lineSelectorRegex),
  brighton: cities.brighton.match(lineSelectorRegex),
  manchester: cities.manchester.match(lineSelectorRegex),
  london: cities.london.match(lineSelectorRegex),
}

const convertedSingularCoordinates = Object.keys(singularCoordinates).reduce((outputObject, key) => {
  const array = singularCoordinates[key].map(coordinateString => {

    const [,, latitude,, longitude,,] = coordinateString.match(/(.*),(.*),(.*),(.*),(.*),(.*)°/)
    return {latitude, longitude}
  })
  if(key === "homes"){
    outputObject[key] = array
  } else {
    outputObject["cities"] = [...outputObject["cities"], {options: array, cityName: key}]
  }
  
  return outputObject
}, {cities: []})

fs.writeFile('utils/coordinateConverter/lib/convertedCoordinates.json', JSON.stringify(convertedSingularCoordinates), (err) => {
  if (err) {
      throw err;
  }
  console.log("Converted coodinates to JSON.");
});
}
// starts()
module.exports = {
  starts
};
