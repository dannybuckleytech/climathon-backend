"use strict";
const { ApolloError } = require("apollo-server");

const handleError = (source, message, code = "ERR000") => {
  console.error(new Date(), `Error::${source}`, message);
  throw new ApolloError("APP_ERROR", code);
};

const getErrorCode = error => {
  if (error.message === "UserId") return "ERR006";
  if (error.message === "EBN") return "ERR008";
  if (error.message === "translate") return "ERR009";
  return "ERR000";
};

module.exports = {
  handleError,
  getErrorCode
};
