const path = require("path");
const nodeExternals = require('webpack-node-externals');

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  entry: './server.js',
  output: {
    path: path.resolve('dist'),
    filename: 'serverBundle.js',
    libraryTarget: 'commonjs2'
  },
  externals: [nodeExternals()], 
  resolve: {
    extensions: [".js", ".json"]
  },
  target: "node"
};