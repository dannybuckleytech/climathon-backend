const path = require("path");

const cleanWebpackPlugin = require("clean-webpack-plugin");
const {merge} = require("webpack-merge");
const nodeExternals = require("webpack-node-externals");

const common = require("./webpack.common");

module.exports = merge(common, {
  devtool: "source-map",
  entry: [path.join(__dirname, "index.js")],
  externals: [nodeExternals({})],
  mode: "development",
  plugins: [new cleanWebpackPlugin.CleanWebpackPlugin()]
});